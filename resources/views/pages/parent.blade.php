<!DOCTYPE html>
<html lang="en">
<!--begin::Head-->
<head>
    <base href="">
    <title>@yield('title')</title>
    <meta charset="utf-8"/>
    <meta name="description"
          content="The most advanced Bootstrap Admin Theme on Themeforest trusted by 100,000 beginners and professionals. Multi-demo, Dark Mode, RTL support and complete React, Angular, Vue, Asp.Net Core, Blazor, Django, Flask &amp; Laravel versions. Grab your copy now and get life-time updates for free."/>
    <meta name="keywords"
          content="Metronic, Bootstrap, Bootstrap 5, Angular, VueJs, React, Asp.Net Core, Blazor, Django, Flask &amp; Laravel, admin themes, web design, figma, web development, free templates, free admin themes, bootstrap theme, bootstrap template, bootstrap dashboard, bootstrap dak mode, bootstrap button, bootstrap datepicker, bootstrap timepicker, fullcalendar, datatables, flaticon"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title"
          content="Metronic - Bootstrap 5 HTML, VueJS, React, Angular, Asp.Net Core, Blazor, Django, Flask &amp; Laravel Admin Dashboard Theme"/>
    <meta property="og:url" content="https://keenthemes.com/metronic"/>
    <meta property="og:site_name" content="Keenthemes | Metronic"/>

    {{--    <meta name="csrf-token" content="{{ csrf_token() }}">--}}

    {{--    <link rel="stylesheet" type="text/css"--}}
    {{--          href="{{asset('assets/plugins/custom/jquery-dataTables/jquery.dataTables.min.css')}}">--}}
    {{--    <link rel="stylesheet" type="text/css"--}}
    {{--          href="{{asset('assets/plugins/custom/dataTables-bootstrap4/dataTables.bootstrap4.min.css')}}">--}}
    {{--    <link rel="stylesheet" href="http://www.fontstatic.com/f=cocon-next-arabic"/>--}}

    <link rel="canonical" href="https://preview.keenthemes.com/metronic8"/>
    <link rel="shortcut icon" href="{{asset('assets/media/logos/favicon.ico')}}"/>
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700"/>
    <!--end::Fonts-->
    <!--begin::Vendor Stylesheets(used by this page)-->
    <link href="{{asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css"/>
    <!--end::Vendor Stylesheets-->
    <!--begin::Global Stylesheets Bundle(used by all pages)-->
    <link href="{{asset('assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/plugins/custom/prismjs/prismjs.bundle.css')}}" rel="stylesheet" type="text/css">
    <!-- Toastr -->
    <link rel="stylesheet" href="{{asset('assets/plugins/custom/toastr/toastr.min.css')}}">

    {{--    <link href="{{asset('assets/plugins/custom/select2/select2.css')}}" rel="stylesheet" type="text/css">--}}
    <!--end::Global Stylesheets Bundle-->

    <!-- Toastr -->
    {{--    <link rel="stylesheet" href="{{asset('assets/plugins/custom/toastr/toastr.min.css')}}">--}}
    @yield('styles')
</head>
<!--end::Head-->
<!--begin::Body-->
<body data-kt-name="metronic" id="kt_app_body" data-kt-app-layout="dark-sidebar" data-kt-app-header-fixed="true"
      data-kt-app-sidebar-enabled="true" data-kt-app-sidebar-fixed="true" data-kt-app-sidebar-hoverable="true"
      data-kt-app-sidebar-push-header="true" data-kt-app-sidebar-push-toolbar="true"
      data-kt-app-sidebar-push-footer="true" data-kt-app-toolbar-enabled="true" class="app-default">
<!--begin::Theme mode setup on page load-->
<script>if (document.documentElement) {
        const defaultThemeMode = "system";
        const name = document.body.getAttribute("data-kt-name");
        let themeMode = localStorage.getItem("kt_" + (name !== null ? name + "_" : "") + "theme_mode_value");
        if (themeMode === null) {
            if (defaultThemeMode === "system") {
                themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
            } else {
                themeMode = defaultThemeMode;
            }
        }
        document.documentElement.setAttribute("data-theme", themeMode);
    }</script>


<div class="d-flex flex-column flex-root app-root" id="kt_app_root">
    <!--begin::Page-->
    <div class="app-page flex-column flex-column-fluid" id="kt_app_page">
        <!--begin::Header-->
        <div id="kt_app_header" class="app-header">
            <!--begin::Header container-->
            @include('../layouts.header')
            <!--end::Header container-->
        </div>

        <!--end::Header-->
        <!--begin::Wrapper-->
        <div class="app-wrapper flex-column flex-row-fluid" id="kt_app_wrapper">

            <!--begin::sidebar-->
            @include('../layouts/sidebar')
            <!--end::sidebar-->


            <!--begin::Main-->
            <!--begin::Content wrapper-->
            <!--begin::Toolbar-->
            @include('../layouts.toolbar')
            <!--end::Toolbar-->
            <!--begin::Content-->
            <!--begin::Content container-->
            @yield('content')
            <!--end::Content container-->
            <!--end::Content-->
            <!--end::Content wrapper-->
            <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
                <!--begin::Footer-->
                @include('../layouts/footer')
                <!--end::Footer-->
            </div>


            <!--end:::Main-->
        </div>
        <!--end::Wrapper-->
    </div>
    <!--end::Page-->
</div>
<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
    <!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
    <span class="svg-icon">
				<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
					<rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)"
                          fill="currentColor"></rect>
					<path
                        d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z"
                        fill="currentColor"></path>
				</svg>
			</span>
    <!--end::Svg Icon-->
</div>

<script>var hostUrl = "assets/";</script>
<!--begin::Global Javascript Bundle(used by all pages)-->
<script src="{{asset('assets/plugins/global/plugins.bundle.js')}}"></script>
<script src="{{asset('assets/js/scripts.bundle.js')}}"></script>
{{--<script src="{{asset('assets/js/custom/authentication/sign-in/general.js')}}"></script>--}}
<!--end::Global Javascript Bundle-->

<!--begin::Vendors Javascript(used by this page)-->
<script src="{{asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js')}}"></script>
<script src="https://cdn.amcharts.com/lib/5/index.js"></script>
<script src="https://cdn.amcharts.com/lib/5/xy.js"></script>
<script src="https://cdn.amcharts.com/lib/5/percent.js"></script>
<script src="https://cdn.amcharts.com/lib/5/radar.js"></script>
<script src="https://cdn.amcharts.com/lib/5/themes/Animated.js"></script>
<script src="https://cdn.amcharts.com/lib/5/map.js"></script>
<script src="https://cdn.amcharts.com/lib/5/geodata/worldLow.js"></script>
<script src="https://cdn.amcharts.com/lib/5/geodata/continentsLow.js"></script>
<script src="https://cdn.amcharts.com/lib/5/geodata/usaLow.js"></script>
<script src="https://cdn.amcharts.com/lib/5/geodata/worldTimeZonesLow.js"></script>
<script src="https://cdn.amcharts.com/lib/5/geodata/worldTimeZoneAreasLow.js"></script>
<script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>
<!--end::Vendors Javascript-->

<!--begin::Custom Javascript(used by this page)-->
<script src="{{asset('assets/js/custom/apps/customers/list/export.js')}}"></script>
<script src="{{asset('assets/js/custom/apps/customers/list/list.js')}}"></script>
<script src="{{asset('assets/js/custom/apps/customers/add.js')}}"></script>
<script src="{{asset('assets/js/widgets.bundle.js')}}"></script>
<script src="{{asset('assets/js/custom/widgets.js')}}"></script>
<script src="{{asset('assets/js/custom/apps/chat/chat.js')}}"></script>
<script src="{{asset('assets/js/custom/utilities/modals/upgrade-plan.js')}}"></script>
<script src="{{asset('assets/js/custom/utilities/modals/create-app.js')}}"></script>
<script src="{{asset('assets/js/custom/utilities/modals/users-search.js')}}"></script>

<!-- jQuery -->
<script src="{{ asset('assets/plugins/custom/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
{{--<script src="{{ asset('cms/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>--}}

<!--end::Custom Javascript-->
<script src="{{asset('js/sweetAlert.js')}}"></script>

<script src="{{asset('js/axios.js')}}"></script>
<!-- Toastr -->
<script src="{{asset('assets/plugins/custom/toastr/toastr.min.js')}}"></script>

<script src="{{asset('js/crud.js')}}"></script>

<!-- jQuery -->
{{--<script src="{{ asset('assets/plugins/custom/jquery/jquery.min.js') }}"></script>--}}
<!-- Bootstrap 4 -->
{{--<script src="{{ asset('assets/plugins/custom/bootstrap/js/bootstrap.bundle.min.js') }}"></script>--}}


{{--<script type="text/javascript" charset="utf8"--}}
{{--        src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.1/jquery.js"></script>--}}
{{--<script type="text/javascript" charset="utf8"--}}
{{--        src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>--}}
{{--<script type="text/javascript" charset="utf8"--}}
{{--        src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>--}}
{{--<script type="text/javascript" charset="utf8"--}}
{{--        src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>--}}
{{--<script type="text/javascript" charset="utf8"--}}
{{--        src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>--}}

<!--begin::Custom Javascript(used by this page)-->
<script src="{{asset('assets/plugins/custom/prismjs/prismjs.bundle.js')}}"></script>

<!--end::Custom Javascript-->

{{--<script src="{{asset('js/sweetAlert.js')}}"></script>--}}

{{--<script src="{{asset('js/axios.js')}}"></script>--}}
{{--<!-- Toastr -->--}}
{{--<script src="{{asset('assets/plugins/custom/toastr/toastr.min.js')}}"></script>--}}

{{--<script src="{{asset('js/crud.js')}}"></script>--}}

@yield('scripts')

<!--end::Javascript-->
</body>
<!--end::Body-->
</html>
