@extends('pages.parent')

@section('title','Dashboard | Home')

@section('page_name','Pages')

@section('main_path','Home')
@section('sub_path','home page')



@section('styles')
@endsection

@section('content')
    <div id="kt_app_content" class="app-content flex-column-fluid"
         data-select2-id="select2-data-kt_app_content">
        <!--begin::Content container-->
        <div id="kt_app_content_container" class="app-container container-xxl">
            <!--begin::Contacts App- Add New Contact-->
            <div class="row g-7" data-select2-id="select2-data-127-s6zt">
                <!--begin::Content-->
                <div class="col-xl-12" data-select2-id="select2-data-126-xy7q">
                    <!--begin::Contacts-->
                    <div class="card card-flush h-lg-100 p-10" id="kt_contacts_main"
                         data-select2-id="select2-data-kt_contacts_main">
                        <!--begin::Card header-->
                        <!--begin::Card title-->
                        <div class="fs-2hx fw-bold text-gray-800 text-center mb-13">
                                            <span class="me-2"> Hello {{ Auth::user()->name }} !! <br/>
                                                <span style="margin-right: 10px"
                                                      class="position-relative d-inline-block text-danger">
                                                    <!--begin::Separator-->
                                                    <span
                                                        class="position-absolute opacity-15 bottom-0 start-0 border-4 border-danger border-bottom w-100"></span>
                                                    <!--end::Separator-->
                                                </span>Welcome in
                                                <a class="text-danger opacity-75-hover">Final Laravel Project</a>
                                            </span>
                        </div>
                        <!--end::Title-->
                        <!--begin::Wrapper-->
                        <!--begin::Illustration-->
                        <img class="mx-auto h-150px h-lg-200px theme-light-show"
                             src="{{asset('assets/media/illustrations/misc/upgrade.svg')}}" alt=""/>
                        <img class="mx-auto h-150px h-lg-200px theme-dark-show"
                             src="{{asset('assets/media/illustrations/misc/upgrade-dark.svg')}}" alt=""/>
                        <!--end::Illustration-->
                    </div>
                    <!--end::Card body-->
                </div>
                <!--end::Contacts-->
            </div>
            <!--end::Content-->
        </div>
        <!--end::Contacts App- Add New Contact-->
    </div>
    <!--end::Content container-->

@endsection

@section('scripts')
@endsection
