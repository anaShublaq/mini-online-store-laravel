@extends('website.parent')

@section('title','Stores');

@section('styles')
@endsection

@section('content')

    <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
        <!--begin::Content wrapper-->
        <div class="d-flex flex-column flex-column-fluid">
            <!--begin::Content-->
            <div id="kt_app_content" class="app-content flex-column-fluid">
                <!--begin::Content container-->
                <div id="kt_app_content_container" class="app-container container-xxl">
                    <!--begin::Home card-->
                    <div class="card" style="margin-top: 5%">
                        <!--begin::Body-->
                        <div style="background-color: #13263c;border-radius: 10px" class="card-body p-lg-10">
                            <!--begin::Section-->
                            <div class="mb-17">
                                <!--begin::Content-->
                                <div class="d-flex flex-stack mb-5">
                                    <!--begin::Title-->
                                    <h3 class="text-dark fs-xl-2hx">Stores</h3>
                                    <!--end::Title-->
                                    <!--begin::Link-->
                                    <a href="{{route('website.home')}}" class="fs-4 fw-semibold link-primary">Go
                                        Home</a>
                                    <!--end::Link-->
                                </div>
                                <!--end::Content-->
                                <!--begin::Separator-->
                                <div class="separator separator-dashed mb-9"></div>
                                <!--end::Separator-->
                                {{$stores->links('pagination/custom')}}
                                <!--begin::Row-->
                                <div class="row justify-content-center mt-xl-8">
                                    @foreach($stores as $store)
                                        <div
                                            style="background-color: whitesmoke;border-radius: 10px;margin-bottom: 20px"
                                            class="col-md-3 mx-5">
                                            <!--begin::Body-->
                                            <div class="card-body pt-15 px-0">
                                                <!--begin::Member-->
                                                <div class="d-flex flex-column text-center mb-9 px-9">
                                                    <!--begin::Photo-->
                                                    <div class="symbol symbol-80px symbol-lg-150px mb-4">
                                                        <img src="{{asset('storage/'.$store->logo)}}" class="" width="auto"
                                                             alt="">
                                                    </div>
                                                    <!--end::Photo-->
                                                    <!--begin::Info-->
                                                    <div class="text-center">
                                                        <!--begin::Name-->
                                                        <a class="fw-bold text-primary fs-4">{{$store->name}}</a>
                                                        <!--end::Name-->
                                                        <!--begin::Position-->
                                                        <span
                                                            class="text-muted d-block fw-semibold">{{$store->address}}</span>
                                                        <!--end::Position-->
                                                    </div>
                                                    <!--end::Info-->
                                                </div>
                                                <!--end::Member-->
                                                <!--begin::Row-->
                                                <div class="row px-9 mb-4 justify-content-center">
                                                    <!--begin::Col-->
                                                    <div class="col-md-4 text-center">
                                                        <div class="text-gray-800 fw-bold fs-3">
                                                        <span class="m-0 counted text-primary" data-kt-countup="true"
                                                              data-kt-countup-value="24"
                                                              data-kt-initialized="1">{{count($store->products)}}</span>
                                                        </div>
                                                        <span
                                                            class="text-gray-500 fs-8 d-block fw-bold">Products</span>
                                                    </div>
                                                    <!--end::Col-->
                                                </div>
                                                <!--end::Row-->
                                                <!--begin::Navbar-->
                                                <div class="m-0">
                                                    <!--begin::Navs-->
                                                    <ul class="nav nav-pills nav-pills-custom flex-column border-transparent fs-5 fw-bold">
                                                        <!--begin::Nav item-->
                                                        <li class="nav-item mt-5">
                                                            <a class="nav-link btn btn-primary"
                                                               href="{{route('website.products',$store->id)}}">
                                                                <!--begin::Svg Icon | path: icons/duotune/general/gen010.svg-->
                                                                <span class="svg-icon svg-icon-3 svg-icon-muted me-3">
																	<svg width="24" height="24" viewBox="0 0 24 24"
                                                                         fill="none" xmlns="http://www.w3.org/2000/svg">
																		<path opacity="0.3"
                                                                              d="M2 21V14C2 13.4 2.4 13 3 13H21C21.6 13 22 13.4 22 14V21C22 21.6 21.6 22 21 22H3C2.4 22 2 21.6 2 21Z"
                                                                              fill="currentColor"></path>
																		<path
                                                                            d="M2 10V3C2 2.4 2.4 2 3 2H21C21.6 2 22 2.4 22 3V10C22 10.6 21.6 11 21 11H3C2.4 11 2 10.6 2 10Z"
                                                                            fill="currentColor"></path>
																	</svg>
																</span>
                                                                <!--end::Svg Icon-->Browse products
                                                                <!--begin::Bullet-->
                                                                <span
                                                                    class="bullet-custom position-absolute start-0 top-0 w-3px h-100 bg-primary rounded-end"></span>
                                                                <!--end::Bullet--></a>
                                                        </li>
                                                        <!--end::Nav item-->
                                                    </ul>
                                                    <!--begin::Navs-->
                                                </div>
                                                <!--end::Navbar-->
                                            </div>
                                            <!--end::Body-->
                                        </div>
                                    @endforeach
                                </div>
                                <!--end::Row-->
                            </div>
                            <!--end::Section-->
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Home card-->
                </div>
                <!--end::Content container-->
            </div>
            <!--end::Content-->
        </div>
        <!--end::Content wrapper-->
    </div>
@endsection


@section('scripts')
@endsection
