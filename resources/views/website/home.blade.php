@extends('website.parent')

@section('title','Home');

@section('styles')
@endsection

@section('landing')
    <!--begin::Landing hero-->
    <div class="d-flex flex-column flex-center w-100 min-h-350px min-h-lg-500px px-9">
        <!--begin::Heading-->
        <div class="text-center mb-5 mb-lg-10 py-10 py-lg-20">
            <!--begin::Title-->
            <h1 class="text-white lh-base fw-bold fs-2x fs-lg-3x mb-15">Final Laravel Project
                <br/>
                <span
                    style="background: linear-gradient(to right, #12CE5D 0%, #FFD80C 100%);-webkit-background-clip: text;-webkit-text-fill-color: transparent;">
								<span id="kt_landing_hero_text">Welcome in Public website</span>
							</span></h1>
            <!--end::Title-->
        </div>
        <!--end::Heading-->
        <!--begin::Clients-->
        <div class="d-flex flex-center flex-wrap position-relative px-5">
            <!--begin::Client-->
            <div class="d-flex flex-center m-3 m-md-6" data-bs-toggle="tooltip" title="Fujifilm">
                <img src="{{asset('assets/media/svg/brand-logos/fujifilm.svg')}}" class="mh-30px mh-lg-40px"
                     alt=""/>
            </div>
            <!--end::Client-->
            <!--begin::Client-->
            <div class="d-flex flex-center m-3 m-md-6" data-bs-toggle="tooltip" title="Vodafone">
                <img src="{{asset('assets/media/svg/brand-logos/vodafone.svg')}}" class="mh-30px mh-lg-40px"
                     alt=""/>
            </div>
            <!--end::Client-->
            <!--begin::Client-->
            <div class="d-flex flex-center m-3 m-md-6" data-bs-toggle="tooltip" title="KPMG International">
                <img src="{{asset('assets/media/svg/brand-logos/kpmg.svg')}}" class="mh-30px mh-lg-40px"
                     alt=""/>
            </div>
            <!--end::Client-->
            <!--begin::Client-->
            <div class="d-flex flex-center m-3 m-md-6" data-bs-toggle="tooltip" title="Nasa">
                <img src="{{asset('assets/media/svg/brand-logos/nasa.svg')}}" class="mh-30px mh-lg-40px"
                     alt=""/>
            </div>
            <!--end::Client-->
            <!--begin::Client-->
            <div class="d-flex flex-center m-3 m-md-6" data-bs-toggle="tooltip" title="Aspnetzero">
                <img src="{{asset('assets/media/svg/brand-logos/aspnetzero.svg')}}" class="mh-30px mh-lg-40px"
                     alt=""/>
            </div>
            <!--end::Client-->
            <!--begin::Client-->
            <div class="d-flex flex-center m-3 m-md-6" data-bs-toggle="tooltip" title="AON - Empower Results">
                <img src="{{asset('assets/media/svg/brand-logos/aon.svg')}}" class="mh-30px mh-lg-40px" alt=""/>
            </div>
            <!--end::Client-->
            <!--begin::Client-->
            <div class="d-flex flex-center m-3 m-md-6" data-bs-toggle="tooltip" title="Hewlett-Packard">
                <img src="{{asset('assets/media/svg/brand-logos/hp-3.svg')}}" class="mh-30px mh-lg-40px"
                     alt=""/>
            </div>
            <!--end::Client-->
            <!--begin::Client-->
            <div class="d-flex flex-center m-3 m-md-6" data-bs-toggle="tooltip" title="Truman">
                <img src="{{asset('assets/media/svg/brand-logos/truman.svg')}}" class="mh-30px mh-lg-40px"
                     alt=""/>
            </div>
            <!--end::Client-->
        </div>
        <!--end::Clients-->
    </div>
    <!--end::Landing hero-->
@endsection

@section('curve')
    <!--begin::Curve bottom-->
    <div class="landing-curve landing-dark-color mb-10 mb-lg-20">
        <svg viewBox="15 12 1470 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
                d="M0 11C3.93573 11.3356 7.85984 11.6689 11.7725 12H1488.16C1492.1 11.6689 1496.04 11.3356 1500 11V12H1488.16C913.668 60.3476 586.282 60.6117 11.7725 12H0V11Z"
                fill="currentColor"></path>
        </svg>
    </div>
    <!--end::Curve bottom-->
@endsection

@section('content')
    <!--begin::Statistics Section-->
    <div class="mt-sm-n10 mt-xl-6">
        <!--begin::Curve top-->
        <div class="landing-curve landing-dark-color">
            <svg viewBox="15 -1 1470 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                    d="M1 48C4.93573 47.6644 8.85984 47.3311 12.7725 47H1489.16C1493.1 47.3311 1497.04 47.6644 1501 48V47H1489.16C914.668 -1.34764 587.282 -1.61174 12.7725 47H1V48Z"
                    fill="currentColor"></path>
            </svg>
        </div>
        <!--end::Curve top-->
        <!--begin::Wrapper-->
        <div id="statistics" class="pb-15 pt-18 landing-dark-bg">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Statistics-->
                <div class="d-flex flex-center">
                    <!--begin::Items-->
                    <div class="d-flex flex-wrap flex-center justify-content-lg-between mb-15 mx-auto w-xl-900px">
                        <!--begin::Item-->
                        <div
                            class="d-flex flex-column flex-center h-200px w-200px h-lg-250px w-lg-250px m-3 bgi-no-repeat bgi-position-center bgi-size-contain"
                            style="background-image: url({{asset('assets/media/svg/misc/octagon.svg')}})">
                            <!--begin::Symbol-->
                            <i class="fa-solid fa-store text-primary fs-4x mb-5"></i>
                            <!--end::Symbol-->
                            <!--begin::Info-->
                            <div class="mb-0">
                                <!--begin::Value-->
                                <div class="fs-lg-2hx fs-2x fw-bold text-white d-flex flex-center">
                                    <div class="min-w-70px text-center" data-kt-countup="true"
                                         data-kt-countup-value="{{$storesCount}}"
                                         data-kt-countup-suffix="+">0
                                    </div>
                                </div>
                                <!--end::Value-->
                                <!--begin::Label-->
                                <span class="text-gray-600 fw-semibold fs-5 lh-0">Stores number</span>
                                <!--end::Label-->
                            </div>
                            <!--end::Info-->
                        </div>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <div
                            class="d-flex flex-column flex-center h-200px w-200px h-lg-250px w-lg-250px m-3 bgi-no-repeat bgi-position-center bgi-size-contain"
                            style="background-image: url({{asset('assets/media/svg/misc/octagon.svg')}})">
                            <!--begin::Symbol-->
                            <i class="bi bi-box-seam text-primary fs-4x mb-5"></i>
                            <!--end::Symbol-->
                            <!--begin::Info-->
                            <div class="mb-0">
                                <!--begin::Value-->
                                <div class="fs-lg-2hx fs-2x fw-bold text-white d-flex flex-center">
                                    <div class="min-w-70px text-center" data-kt-countup="true"
                                         data-kt-countup-value="{{$productsCount}}"
                                         data-kt-countup-suffix="+">0
                                    </div>
                                </div>
                                <!--end::Value-->
                                <!--begin::Label-->
                                <span class="text-gray-600 fw-semibold fs-5 lh-0">Products number</span>
                                <!--end::Label-->
                            </div>
                            <!--end::Info-->
                        </div>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <div
                            class="d-flex flex-column flex-center h-200px w-200px h-lg-250px w-lg-250px m-3 bgi-no-repeat bgi-position-center bgi-size-contain"
                            style="background-image: url({{asset('assets/media/svg/misc/octagon.svg')}})">
                            <!--begin::Symbol-->
                            <i class="las la-wallet text-primary fs-4x mb-5"></i>
                            <!--end::Symbol-->
                            <!--begin::Info-->
                            <div class="mb-0">
                                <!--begin::Value-->
                                <div class="fs-lg-2hx fs-2x fw-bold text-white d-flex flex-center">
                                    <div class="min-w-70px text-center" data-kt-countup="true"
                                         data-kt-countup-value="{{$purchaseTransactions}}"
                                         data-kt-countup-suffix="+">0
                                    </div>
                                </div>
                                <!--end::Value-->
                                <!--begin::Label-->
                                <span class="text-gray-600 fw-semibold fs-5 lh-0">Purchases number</span>
                                <!--end::Label-->
                            </div>
                            <!--end::Info-->
                        </div>
                        <!--end::Item-->
                    </div>
                    <!--end::Items-->
                </div>
                <!--end::Statistics-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Wrapper-->
        <!--begin::Curve bottom-->
        <div class="landing-curve landing-dark-color">
            <svg viewBox="15 12 1470 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                    d="M0 11C3.93573 11.3356 7.85984 11.6689 11.7725 12H1488.16C1492.1 11.6689 1496.04 11.3356 1500 11V12H1488.16C913.668 60.3476 586.282 60.6117 11.7725 12H0V11Z"
                    fill="currentColor"></path>
            </svg>
        </div>
        <!--end::Curve bottom-->
    </div>
    <!--end::Statistics Section-->
@endsection


@section('scripts')
@endsection
