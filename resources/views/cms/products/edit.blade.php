@extends('pages.parent')

@section('title','Update Product')

@section('page_name','CMS')

@section('main_path','Products')
@section('sub_path','Update')



@section('styles')
@endsection

@section('content')
    <!--begin::Main-->
    <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
        <!--begin::Content wrapper-->
        <div class="d-flex flex-column flex-column-fluid">
            <!--begin::Content-->
            <div id="kt_app_content" class="app-content flex-column-fluid">
                <!--begin::Content container-->
                <div id="kt_app_content_container" class="app-container container-fluid">
                    <!--begin::Row-->
                    <div class="row">
                        <!--begin::Col-->
                        <div class="col-l-6">
                            <!--begin::Engage widget 10-->
                            <div class="card card-flush h-md-100">
                                <!--begin::Body-->
                                <div class="col-md-6">
                                    <!--begin::Contacts-->
                                    <div class="card card-flush h-lg-100" id="kt_contacts_main">
                                        <!--begin::Card header-->
                                        <div class="card-header pt-7" id="kt_chat_contacts_header">
                                            <!--begin::Card title-->
                                            <div class="card-title">
                                                <!--begin::Svg Icon | path: icons/duotune/communication/com005.svg-->
                                                <span class="svg-icon svg-icon-1 me-2">
															<i class="bi bi-box-seam text-primary fs-4x mb-5"></i>
														</span>
                                                <!--end::Svg Icon-->
                                                <h2>Update {{$product->name}} Product</h2>
                                            </div>
                                            <!--end::Card title-->
                                        </div>
                                        <!--end::Card header-->
                                        <!--begin::Card body-->
                                        <div class="card-body pt-5">
                                            <!--begin::Form-->
                                            <form class="form fv-plugins-bootstrap5 fv-plugins-framework">
                                                @csrf

                                                <!--begin::Input group-->
                                                <div class="mb-7">
                                                    <!--begin::Label-->
                                                    <label class="fs-6 fw-semibold mb-3">
                                                        <span>Logo</span>
                                                        <i class="fas fa-exclamation-circle ms-1 fs-7"
                                                           data-bs-toggle="tooltip"
                                                           aria-label="Allowed file types: png, jpg, jpeg."
                                                           data-kt-initialized="1"></i>
                                                    </label>
                                                    <!--end::Label-->
                                                    <!--begin::Image input wrapper-->
                                                    <div class="mt-1">
                                                        <!--begin::Image placeholder-->
                                                        <style>.image-input-placeholder {
                                                                background-image: url({{asset('assets/media/svg/files/blank-image.svg')}});
                                                            }

                                                            [data-theme="dark"] .image-input-placeholder {
                                                                background-image: url({{asset('assets/media/svg/files/blank-image-dark.svg')}});
                                                            }</style>
                                                        <!--end::Image placeholder-->
                                                        <!--begin::Image input-->
                                                        <div
                                                            class="image-input image-input-outline image-input-placeholder image-input-empty image-input-empty"
                                                            data-kt-image-input="true">
                                                            <!--begin::Preview existing avatar-->
                                                            <div class="image-input-wrapper w-100px h-100px"></div>
                                                            <!--end::Preview existing avatar-->
                                                            <!--begin::Edit-->
                                                            <label
                                                                class="btn btn-icon btn-circle btn-active-color-primary bg-body shadow"
                                                                data-kt-image-input-action="change"
                                                                data-bs-toggle="tooltip" data-kt-initialized="1">
                                                                <i class="bi bi-pencil-fill fs-7"></i>
                                                                <!--begin::Inputs-->
                                                                <input type="file" name="logo" id="logo">
                                                                <input type="hidden" name="logo_remove">
                                                                <!--end::Inputs-->
                                                            </label>
                                                            <!--end::Edit-->
                                                            <!--begin::Cancel-->
                                                            <span
                                                                class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow"
                                                                data-kt-image-input-action="cancel"
                                                                data-bs-toggle="tooltip" data-kt-initialized="1">
																		<i class="bi bi-x fs-2"></i>
																	</span>
                                                            <!--end::Cancel-->
                                                            <!--begin::Remove-->
                                                            <span
                                                                class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow"
                                                                data-kt-image-input-action="remove"
                                                                data-bs-toggle="tooltip" data-kt-initialized="1">
																		<i class="bi bi-x fs-2"></i>
																	</span>
                                                            <!--end::Remove-->
                                                        </div>
                                                        <!--end::Image input-->
                                                    </div>
                                                    <!--end::Image input wrapper-->
                                                </div>
                                                <!--end::Input group-->
                                                <!--begin::Input group-->
                                                <div class="fv-row mb-7 fv-plugins-icon-container">
                                                    <!--begin::Label-->
                                                    <label class="fs-6 fw-semibold form-label mt-3">
                                                        <span class="required">Name</span>
                                                        <i class="fas fa-exclamation-circle ms-1 fs-7"
                                                           data-bs-toggle="tooltip"
                                                           data-kt-initialized="1"></i>
                                                    </label>
                                                    <!--end::Label-->
                                                    <!--begin::Input-->
                                                    <input id="name" type="text" class="form-control form-control-solid"
                                                           name="name" value="{{$product->name}}">
                                                    <!--end::Input-->
                                                    <div class="fv-plugins-message-container invalid-feedback"></div>
                                                </div>
                                                <!--end::Input group-->
                                                <!--begin::Input group-->
                                                <div class="fv-row mb-7">
                                                    <!--begin::Label-->
                                                    <label class="fs-6 fw-semibold form-label mt-3">
                                                        <span>Description</span>
                                                        <i class="fas fa-exclamation-circle ms-1 fs-7"
                                                           data-bs-toggle="tooltip"
                                                           data-kt-initialized="1"></i>
                                                    </label>
                                                    <!--end::Label-->
                                                    <!--begin::Input-->
                                                    <textarea id="description" class="form-control form-control-solid"
                                                              name="description"
                                                              rows="4">{{$product->description}}</textarea>
                                                    <!--end::Input-->
                                                </div>
                                                <!--end::Input group-->

                                                <!--begin::Input group-->
                                                <div class="fv-row mb-20">
                                                    <!--begin::Label-->
                                                    <label class="fs-6 fw-semibold form-label mt-3">
                                                        <span class="required">Store Name</span>
                                                        <i class="fas fa-exclamation-circle ms-1 fs-7"
                                                           data-bs-toggle="tooltip"
                                                           data-kt-initialized="1"></i>
                                                    </label>
                                                    <!--end::Label-->
                                                    <select id="storeName" class="form-select form-select-solid"
                                                            data-control="select2"
                                                            data-placeholder="Select an option" data-allow-clear="true">
                                                        {{$stores = \App\Models\Store::all()}}
                                                        <option>Choose Store</option>
                                                        <option value="{{$product->store->id}}"
                                                                selected>{{$product->store->name}}</option>
                                                        @foreach($stores as $store)
                                                            <option value="{{$store->id}}">{{$store->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <!--end::Input group-->

                                                <!--begin::Input group-->
                                                <div class="fv-row mb-7 fv-plugins-icon-container priceSlider">
                                                    <!--begin::Label-->
                                                    <label class="fs-6 fw-semibold mb-2">
                                                        <span class="required">Base Price</span>
                                                        <i class="fas fa-exclamation-circle fs-7"
                                                           data-bs-toggle="tooltip"></i></label>
                                                    <!--end::Label-->
                                                    <!--begin::Slider-->
                                                    <div class="d-flex flex-column text-center">
                                                        <div
                                                            class="d-flex align-items-start justify-content-center mb-7">
                                                            <span class="fw-bold fs-4 mt-1 me-2">$</span>
                                                            <span class="fw-bold fs-3x"
                                                                  id="basePriceLabel"></span>
                                                            <span class="fw-bold fs-3x">.00</span>
                                                        </div>
                                                        <div id="basePriceSlider"
                                                             class="noUi-sm"></div>
                                                    </div>
                                                    <!--end::Slider-->
                                                    <div class="fv-plugins-message-container invalid-feedback"></div>
                                                </div>
                                                <!--end::Input group-->

                                                <!--begin::Input group-->
                                                <div class="d-flex flex-stack w-lg-50 pt-20 pb-10">
                                                    <!--begin::Switch-->
                                                    <label
                                                        class="form-check form-switch form-check-custom form-check-solid">
                                                        <input id="hasDiscountPrice"
                                                               class="form-check-input hasDiscount"
                                                               type="checkbox" value="1" onchange="valueChanged()"/>
                                                        <span class="form-check-label fw-semibold text-muted">Has a Discount ?</span>
                                                    </label>
                                                    <!--end::Switch-->
                                                </div>


                                                <!--begin::Input group-->
                                                <div style="display: none"
                                                     class="fv-row mb-7 fv-plugins-icon-container discountPriceSlider">
                                                    <!--begin::Label-->
                                                    <label class="fs-6 fw-semibold mb-2">Discount Price
                                                        <i class="fas fa-exclamation-circle ms-2 fs-7"
                                                           data-bs-toggle="tooltip"></i></label>
                                                    <!--end::Label-->
                                                    <!--begin::Slider-->
                                                    <div class="d-flex flex-column text-center">
                                                        <div
                                                            class="d-flex align-items-start justify-content-center mb-7">
                                                            <span class="fw-bold fs-4 mt-1 me-2">$</span>
                                                            <span class="fw-bold fs-3x"
                                                                  id="discountPriceLabel"></span>
                                                            <span class="fw-bold fs-3x">.00</span>
                                                        </div>
                                                        <div id="discountPriceSlider"
                                                             class="noUi-sm"></div>
                                                    </div>
                                                    <!--end::Slider-->
                                                    <div class="fv-plugins-message-container invalid-feedback"></div>
                                                </div>
                                                <!--end::Input group-->

                                                <!--begin::Separator-->
                                                <div class="separator mb-6"></div>
                                                <!--end::Separator-->
                                                <!--begin::Action buttons-->
                                                <div class="d-flex justify-content-end">
                                                    <!--begin::Button-->
                                                    <button onclick="redirectBack()"
                                                            data-kt-contacts-type="cancel"
                                                            class="btn btn-light me-3">Back
                                                    </button>
                                                    <!--end::Button-->
                                                    <!--begin::Button-->
                                                    <button id="sendRequest" type="button"
                                                            data-kt-contacts-type="submit"
                                                            class="btn btn-primary" onclick="performUpdate()">
                                                        <span class="indicator-label">Save</span>
                                                        <span class="indicator-progress">Please wait...
																<span
                                                                    class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                                    </button>
                                                    <!--end::Button-->
                                                </div>
                                                <!--end::Action buttons-->
                                                <div></div>
                                            </form>
                                            <!--end::Form-->
                                        </div>
                                        <!--end::Card body-->
                                    </div>
                                    <!--end::Contacts-->
                                </div>
                                <!--end::Body-->
                            </div>
                            <!--end::Engage widget 10-->
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Row-->
                </div>
                <!--end::Content container-->
            </div>
            <!--end::Content-->
        </div>
        <!--end::Content wrapper-->
    </div>
    <!--end:::Main-->
@endsection

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>

    <script>
        function valueChanged() {
            if ($('.hasDiscount').is(":checked"))
                $(".discountPriceSlider").show();
            else
                $(".discountPriceSlider").hide();
        }

        setSliders();
    </script>


    <!--begin::Vendors Javascript(used by this page)-->
    <script src="{{asset('assets/plugins/custom/prismjs/prismjs.bundle.js')}}"></script>
    <!--end::Vendors Javascript-->
    <!--begin::Custom Javascript(used by this page)-->
    <script src="{{asset('assets/js/custom/documentation/documentation.js')}}"></script>
    <script src="{{asset('assets/js/custom/documentation/search.js')}}"></script>
    <script src="{{asset('assets/js/custom/documentation/forms/select2.js')}}"></script>

    <script>
        function performUpdate() {

            $('#sendRequest').addClass("disabled");

            let isChecked = document.getElementById('hasDiscountPrice').checked;
            let discountPrice = 0;

            if (isChecked) {
                discountPrice = parseInt(document.getElementById('discountPriceLabel').textContent);
            }


            let formData = new FormData();
            formData.append('_method', 'PUT');
            formData.append('name', document.getElementById('name').value);
            formData.append('description', document.getElementById('description').value);
            formData.append('storeId', document.getElementById('storeName').value);
            formData.append('basePriceLabel', document.getElementById('basePriceLabel').textContent);
            formData.append('hasDiscountPrice', isChecked);
            formData.append('discountPriceLabel', document.getElementById('discountPriceLabel').textContent);
            formData.append('logo', document.getElementById('logo').files[0]);

            update('/cms/product/{{$product->id}}', formData, '/cms/product');
        }

        function redirectBack() {
            redirect('/cms/product');
        }
    </script>
@endsection
