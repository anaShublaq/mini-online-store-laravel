@extends('pages.parent')

@section('title','Show Stores')

@section('page_name','CMS')

@section('main_path','Stores')
@section('sub_path','Show')



@section('styles')
@endsection

@section('content')
    <!--begin::Main-->
    <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
        <!--begin::Content wrapper-->
        <div class="d-flex flex-column flex-column-fluid">
            <!--begin::Content-->
            <div id="kt_app_content" class="app-content flex-column-fluid">
                <!--begin::Content container-->
                <div id="kt_app_content_container" class="app-container container-fluid">
                    <!--begin::Row-->
                    <div class="row g-5 g-xl-10 mb-5 mb-xl-10">
                        <!--begin::Col-->
                        <div class="col-xxl-6">
                            <!--begin::Engage widget 10-->
                            <div class="card card-flush h-md-100">
                                <!--begin::Body-->
                                <div class="card" data-select2-id="select2-data-133-rqx2">
                                    <!--begin::Card header-->
                                    <div class="card-header border-0 pt-6" data-select2-id="select2-data-132-1r82">
                                        <!--begin::Card title-->
                                        <div class="card-title">
                                        </div>
                                        <!--begin::Card title-->
                                        <!--begin::Card toolbar-->
                                        <div class="card-toolbar" data-select2-id="select2-data-131-mcw4">
                                            <!--begin::Toolbar-->
                                            <div class="d-flex justify-content-end"
                                                 data-kt-customer-table-toolbar="base"
                                                 data-select2-id="select2-data-130-vrh1">
                                                <!--begin::Add customer-->
                                                <a href="{{route('store.create')}}" class="btn btn-primary"
                                                   data-bs-target="#kt_modal_add_customer">Add Store
                                                </a>
                                                <!--end::Add customer-->
                                            </div>
                                            <!--end::Toolbar-->
                                            <!--begin::Group actions-->
                                            <div class="d-flex justify-content-end align-items-center d-none"
                                                 data-kt-customer-table-toolbar="selected">
                                                <div class="fw-bold me-5">
                                                    <span class="me-2"
                                                          data-kt-customer-table-select="selected_count">10</span>Selected
                                                </div>
                                                <button type="button" class="btn btn-danger"
                                                        data-kt-customer-table-select="delete_selected">Delete Selected
                                                </button>
                                            </div>
                                            <!--end::Group actions-->
                                        </div>
                                        <!--end::Card toolbar-->
                                    </div>
                                    <!--end::Card header-->
                                    <!--begin::Card body-->
                                    <br>
                                    <div class="card-body pt-0">
                                        <!--begin::Table-->
                                        <div id="kt_customers_table_wrapper"
                                             class="dataTables_wrapper dt-bootstrap4 no-footer">
                                            <div class="table-responsive">
                                                <table
                                                    class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer"
                                                    id="kt_customers_table">
                                                    <!--begin::Table head-->
                                                    <thead>
                                                    <!--begin::Table row-->
                                                    <tr class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                                                        <th class="min-w-100px text-center" tabindex="0"
                                                            aria-controls="kt_customers_table" rowspan="1" colspan="1"
                                                            style="width: 148.266px;">Logo
                                                        </th>
                                                        <th class="min-w-100px text-center" tabindex="0"
                                                            aria-controls="kt_customers_table" rowspan="1" colspan="1"
                                                            style="width: 140.266px;">Name
                                                        </th>
                                                        <th class="min-w-300px text-center" tabindex="0"
                                                            aria-controls="kt_customers_table" rowspan="1" colspan="1"
                                                            style="width: 164.344px;">Address
                                                        </th>
                                                        <th class="min-w-70px text-center" rowspan="1"
                                                            colspan="1" aria-label="Actions" style="width: 103.797px;">
                                                            Actions
                                                        </th>
                                                    </tr>
                                                    <!--end::Table row-->
                                                    </thead>
                                                    <!--end::Table head-->
                                                    <!--begin::Table body-->
                                                    <tbody class="fw-semibold text-gray-600">
                                                    @foreach($stores as $store)
                                                        <tr>
                                                            <td class="text-center">
                                                                <img width="75px"
                                                                     src="{{asset('storage/'.$store->logo)}}"
                                                                     alt="logo">
                                                            </td>
                                                            <td class="text-center">
                                                                <a
                                                                    class="text-gray-800 text-hover-primary mb-1">{{$store->name}}</a>
                                                            </td>
                                                            <td class="text-center">
                                                                <a
                                                                    class="text-gray-600 text-hover-primary mb-1">{{$store->address}}</a>
                                                            </td>
                                                            <td class="text-center">
                                                                <a class="btn btn-sm btn-light btn-active-light-primary"
                                                                   data-kt-menu-trigger="click"
                                                                   data-kt-menu-placement="bottom-end">Actions
                                                                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr072.svg-->
                                                                    <span class="svg-icon svg-icon-5 m-0">
																<svg width="24" height="24" viewBox="0 0 24 24"
                                                                     fill="none" xmlns="http://www.w3.org/2000/svg">
																	<path
                                                                        d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z"
                                                                        fill="currentColor"></path>
																</svg>
															</span>
                                                                    <!--end::Svg Icon--></a>
                                                                <!--begin::Menu-->
                                                                <div
                                                                    class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-125px py-4"
                                                                    data-kt-menu="true" style="">
                                                                    <!--begin::Menu item-->
                                                                    <div class="menu-item px-3">
                                                                        <a href="{{route('store.edit',$store->id)}}"
                                                                           class="menu-link px-3">Edit</a>
                                                                    </div>
                                                                    <!--end::Menu item-->
                                                                    <!--begin::Menu item-->
                                                                    <div class="menu-item px-3">
                                                                        <a onclick="deleteStore({{$store->id}},this)"
                                                                           class="menu-link px-3"
                                                                           data-kt-customer-table-filter="delete_row">Delete</a>
                                                                    </div>
                                                                    <!--end::Menu item-->
                                                                </div>
                                                                <!--end::Menu-->
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                    <!--end::Table body-->
                                                </table>
                                                <br>
                                                {{$stores->links('pagination.custom')}}
                                            </div>
                                        </div>
                                        <!--end::Table-->
                                    </div>
                                    <!--end::Card body-->
                                </div>
                                <!--end::Body-->
                            </div>
                            <!--end::Engage widget 10-->
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Row-->
                </div>
                <!--end::Content container-->
            </div>
            <!--end::Content-->
        </div>
        <!--end::Content wrapper-->
    </div>
    <!--end:::Main-->
@endsection

@section('scripts')
    <script>
        function deleteStore(id, reference) {
            confirmDestroy('/cms/store', id, reference);
        }
    </script>
@endsection
