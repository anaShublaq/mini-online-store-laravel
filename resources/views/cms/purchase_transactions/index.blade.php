@extends('pages.parent')

@section('title','Show Purchase Transactions')

@section('page_name','CMS')

@section('main_path','Purchase Transactions')
@section('sub_path','Show')



@section('styles')
@endsection

@section('content')
    <!--begin::Main-->
    <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
        <!--begin::Content wrapper-->
        <div class="d-flex flex-column flex-column-fluid">
            <!--begin::Content-->
            <div id="kt_app_content" class="app-content flex-column-fluid">
                <!--begin::Content container-->
                <div id="kt_app_content_container" class="app-container container-fluid">
                    <!--begin::Row-->
                    <div class="row g-5 g-xl-10 mb-5 mb-xl-10">
                        <!--begin::Col-->
                        <div class="col-xxl-6">
                            <!--begin::Engage widget 10-->
                            <div class="card card-flush h-md-100">
                                <!--begin::Body-->
                                <div class="card" data-select2-id="select2-data-133-rqx2">
                                    <!--begin::Card header-->
                                    <div class="card-header border-0 pt-6" data-select2-id="select2-data-132-1r82">
                                        <!--begin::Card title-->
                                        <div class="card-title">
                                        </div>
                                        <!--begin::Card title-->
                                    </div>
                                    <!--end::Card header-->
                                    <!--begin::Card body-->
                                    <br>
                                    <div class="card-body pt-0">
                                        <!--begin::Table-->
                                        <div id="kt_customers_table_wrapper"
                                             class="dataTables_wrapper dt-bootstrap4 no-footer">
                                            <div class="table-responsive">
                                                <table
                                                    class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer"
                                                    id="kt_customers_table">
                                                    <!--begin::Table head-->
                                                    <thead>
                                                    <!--begin::Table row-->
                                                    <tr class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                                                        <th class="min-w-100px text-center" tabindex="0"
                                                            aria-controls="kt_customers_table" rowspan="1" colspan="1"
                                                            style="width: 140.266px;">Store Name
                                                        </th>
                                                        <th class="min-w-200px text-center" tabindex="0"
                                                            aria-controls="kt_customers_table" rowspan="1" colspan="1"
                                                            style="width: 164.344px;">Product Name
                                                        </th>
                                                        <th class="min-w-50px text-center" tabindex="0"
                                                            aria-controls="kt_customers_table" rowspan="1" colspan="1"
                                                            style="width: 148.266px;">Purchase Price
                                                        </th>
                                                        <th class="min-w-50px text-center" tabindex="0"
                                                            aria-controls="kt_customers_table" rowspan="1" colspan="1"
                                                            style="width: 148.266px;">Transaction Time
                                                        </th>
                                                    </tr>
                                                    <!--end::Table row-->
                                                    </thead>
                                                    <!--end::Table head-->
                                                    <!--begin::Table body-->
                                                    <tbody class="fw-semibold text-gray-600">
                                                    @foreach($purchase_transactions as $purchase_transaction)
                                                        <tr>
                                                            <td class="text-center">
                                                                <a
                                                                    class="text-gray-800 text-hover-primary mb-1">{{$purchase_transaction->store_name}}</a>
                                                            </td>
                                                            <td class="text-center">
                                                                <a
                                                                    class="text-gray-600 text-hover-primary mb-1">{{$purchase_transaction->product_name}}</a>
                                                            </td>
                                                            <td class="text-center">
                                                                <a class="text-gray-600 text-hover-primary mb-1">
                                                                    <span
                                                                        class="badge badge-light-success fs-base">$ {{$purchase_transaction->purchase_price}}</span>
                                                                </a>
                                                            </td>
                                                            <td class="text-center">
                                                                <a class="text-gray-600 text-hover-primary mb-1">
                                                                    {{$purchase_transaction->transaction_time}}
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                    <!--end::Table body-->
                                                </table>
                                                <br>
                                                {{$purchase_transactions->links('pagination.custom')}}
                                            </div>
                                        </div>
                                        <!--end::Table-->
                                    </div>
                                    <!--end::Card body-->
                                </div>
                                <!--end::Body-->
                            </div>
                            <!--end::Engage widget 10-->
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Row-->
                </div>
                <!--end::Content container-->
            </div>
            <!--end::Content-->
        </div>
        <!--end::Content wrapper-->
    </div>
    <!--end:::Main-->
@endsection

@section('scripts')
@endsection
