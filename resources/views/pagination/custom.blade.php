@if ($paginator->hasPages())
    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
            @if ($paginator->onFirstPage())
                <li class="paginate_button page-item previous disabled"
                    id="kt_customers_table_previous"><a href="#"
                                                        aria-controls="kt_customers_table"
                                                        data-dt-idx="0"
                                                        tabindex="0"
                                                        class="page-link"><i
                            class="previous"></i></a></li>
            @else
                <li class="paginate_button page-item previous"
                    id="kt_customers_table_previous"><a href="{{ $paginator->previousPageUrl() }}"
                                                        aria-controls="kt_customers_table"
                                                        data-dt-idx="0"
                                                        tabindex="0"
                                                        class="page-link"><i
                            class="previous"></i></a></li>
            @endif

            @foreach ($elements as $element)
                @if (is_string($element))
                    <li class="page-item disabled">{{ $element }}</li>
                @endif

                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="page-item active">
                                <a class="page-link">{{ $page }}</a>
                            </li>
                        @else
                            <li class="page-item">
                                <a class="page-link"
                                   href="{{ $url }}">{{ $page }}</a>
                            </li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            @if ($paginator->hasMorePages())
                <li class="paginate_button page-item next"
                    id="kt_customers_table_next"><a href="{{ $paginator->nextPageUrl() }}"
                                                    aria-controls="kt_customers_table"
                                                    data-dt-idx="5"
                                                    tabindex="0"
                                                    class="page-link"><i
                            class="next"></i></a></li>
            @else
                <li class="paginate_button page-item next disabled"
                    id="kt_customers_table_next"><a href="#"
                                                    aria-controls="kt_customers_table"
                                                    data-dt-idx="5"
                                                    tabindex="0"
                                                    class="page-link"><i
                            class="next"></i></a></li>
            @endif
        </ul>
    </nav>
@endif
