<div id="kt_app_header" class="app-header">
    <div class="app-container container-fluid d-flex align-items-stretch justify-content-between">
        <!--begin::sidebar mobile toggle-->
        <div class="d-flex align-items-center d-lg-none ms-n2 me-2" title="Show sidebar menu">
            <div class="btn btn-icon btn-active-color-primary w-35px h-35px" id="kt_app_sidebar_mobile_toggle">
                <!--begin::Svg Icon | path: icons/duotune/abstract/abs015.svg-->
                <span class="svg-icon svg-icon-1">
									<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
										<path
                                            d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z"
                                            fill="currentColor"></path>
										<path opacity="0.3"
                                              d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z"
                                              fill="currentColor"></path>
									</svg>
								</span>
                <!--end::Svg Icon-->
            </div>
        </div>
        <!--end::sidebar mobile toggle-->
        <!--begin::Mobile logo-->
        <div class="d-flex align-items-center flex-grow-1 flex-lg-grow-0">
            <a href="" class="d-lg-none">
                <img alt="Logo" src="{{asset('assets/media/logos/default-small.svg')}}" class="h-30px">
            </a>
        </div>
        <!--end::Mobile logo-->
        <!--begin::Header wrapper-->
        <div class="d-flex align-items-stretch justify-content-between flex-lg-grow-1" id="kt_app_header_wrapper">
            <!--begin::Menu wrapper-->
            <div class="app-header-menu app-header-mobile-drawer align-items-stretch" data-kt-drawer="true"
                 data-kt-drawer-name="app-header-menu" data-kt-drawer-activate="{default: true, lg: false}"
                 data-kt-drawer-width="225px" data-kt-drawer-direction="end"
                 data-kt-drawer-toggle="#kt_app_header_menu_toggle" data-kt-swapper="true"
                 data-kt-swapper-mode="{default: 'append', lg: 'prepend'}"
                 data-kt-swapper-parent="{default: '#kt_app_body', lg: '#kt_app_header_wrapper'}">
                <!--begin::Menu-->
                <div
                    class="menu menu-rounded menu-column menu-lg-row my-5 my-lg-0 align-items-stretch fw-semibold px-2 px-lg-0"
                    id="kt_app_header_menu" data-kt-menu="true">
                    {{--                    @include('layouts.menu')--}}
                </div>
                <!--end::Menu-->
            </div>
            <!--end::Menu wrapper-->
            @include('layouts.navbar')
        </div>
        <!--end::Header wrapper-->
    </div>
</div>

<!--begin::Chat drawer-->
<div dir="rtl" id="kt_drawer_chat" class="bg-body" data-kt-drawer="true" data-kt-drawer-name="chat"
     data-kt-drawer-activate="true" data-kt-drawer-width="{default:'300px', 'md': '500px'}"
     data-kt-drawer-direction="end" data-kt-drawer-toggle="#kt_drawer_chat_toggle"
     data-kt-drawer-close="#kt_drawer_chat_close">
    <!--begin::Messenger-->
    <div class="card w-100 rounded-0 border-0" id="kt_drawer_chat_messenger">
        <!--begin::Card header-->
        <div class="card-header pe-5" id="kt_drawer_chat_messenger_header">
            <!--begin::Title-->
            <div class="card-title">
                <!--begin::User-->
                <div class="d-flex justify-content-center me-3">
                    <a href="#" class="fs-4 fw-bold text-gray-900 text-hover-primary me-1 mb-2 lh-1"></a>
                    <!--begin::Info-->
                    <div style="margin-right: 5px">
                        <span class="badge badge-success badge-circle w-10px h-10px me-1"></span>
                        <span class="fs-7 fw-semibold text-muted">نشط</span>
                    </div>
                    <!--end::Info-->
                </div>
                <!--end::User-->
            </div>
            <!--end::Title-->
            <!--begin::Card toolbar-->
            <div class="card-toolbar">
                <!--begin::Close-->
                <div class="btn btn-sm btn-icon btn-active-light-primary" id="kt_drawer_chat_close">
                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                    <span class="svg-icon svg-icon-2">
								<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
									<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                                          transform="rotate(-45 6 17.3137)" fill="currentColor"/>
									<rect x="7.41422" y="6" width="16" height="2" rx="1"
                                          transform="rotate(45 7.41422 6)" fill="currentColor"/>
								</svg>
							</span>
                    <!--end::Svg Icon-->
                </div>
                <!--end::Close-->
            </div>
            <!--end::Card toolbar-->
        </div>
        <!--end::Card header-->
        <!--begin::Card body-->
        <div class="card-body" id="kt_drawer_chat_messenger_body">
            <!--begin::Messages-->
            <div class="scroll-y me-n5 pe-5" data-kt-element="messages" data-kt-scroll="true"
                 data-kt-scroll-activate="true" data-kt-scroll-height="auto"
                 data-kt-scroll-dependencies="#kt_drawer_chat_messenger_header, #kt_drawer_chat_messenger_footer"
                 data-kt-scroll-wrappers="#kt_drawer_chat_messenger_body" data-kt-scroll-offset="0px">


                <!--begin::Message(in)-->
                <div class="d-flex justify-content-start mb-10">
                    <!--begin::Wrapper-->
                    <div class="d-flex flex-column align-items-start">
                        <!--begin::User-->
                        <div class="d-flex align-items-center mb-2">
                            <!--begin::Details-->
                            <div class="ms-3">
                                <a href="#" class="fs-5 fw-bold text-gray-900 text-hover-primary me-1">اسم المستخدم</a>
                                <span style="margin-right: 5px" class="text-muted fs-7 mb-1">3 دقائق</span>
                            </div>
                            <!--end::Details-->
                        </div>
                        <!--end::User-->
                        <!--begin::Text-->
                        <div class="p-5 rounded bg-light-info text-dark fw-semibold mw-lg-400px"
                             data-kt-element="message-text">اشعار جديد اشعار جديد اشعار جديد اشعار جديد اشعار جديد اشعار
                            جديد اشعار جديد اشعار جديد !!
                        </div>
                        <!--end::Text-->
                        <div style="display: inline-block; margin-top: 10px">
                            <a href="" class="btn btn-primary"><i style="margin: 0 -10px 0 5px"
                                                                  class="fonticon-pin fs-4"></i>
                                اعلام كمقروء</a>
                            <a href="" class="btn btn-primary">
                                <i style="margin: 0 -10px 0 5px" class="fonticon-stats fs-4"></i>
                                التفاصيل</a>
                        </div>
                    </div>
                    <!--end::Wrapper-->
                </div>
                <!--end::Message(in)-->

                <!--begin::Message(in)-->
                <div class="d-flex justify-content-start mb-10">
                    <!--begin::Wrapper-->
                    <div class="d-flex flex-column align-items-start">
                        <!--begin::User-->
                        <div class="d-flex align-items-center mb-2">
                            <!--begin::Details-->
                            <div class="ms-3">
                                <a href="#" class="fs-5 fw-bold text-gray-900 text-hover-primary me-1">اسم المستخدم</a>
                                <span style="margin-right: 5px" class="text-muted fs-7 mb-1">3 دقائق</span>
                            </div>
                            <!--end::Details-->
                        </div>
                        <!--end::User-->
                        <!--begin::Text-->
                        <div class="p-5 rounded bg-light-info text-dark fw-semibold mw-lg-400px"
                             data-kt-element="message-text">اشعار جديد اشعار جديد اشعار جديد اشعار جديد اشعار جديد اشعار
                            جديد اشعار جديد اشعار جديد !!
                        </div>
                        <!--end::Text-->
                        <div style="display: inline-block; margin-top: 10px">
                            <a href="" class="btn btn-primary"><i style="margin: 0 -10px 0 5px"
                                                                  class="fonticon-pin fs-4"></i>
                                اعلام كمقروء</a>
                            <a href="" class="btn btn-primary">
                                <i style="margin: 0 -10px 0 5px" class="fonticon-stats fs-4"></i>
                                التفاصيل</a>
                        </div>
                    </div>
                    <!--end::Wrapper-->
                </div>
                <!--end::Message(in)-->
                <!--begin::Message(in)-->
                <div class="d-flex justify-content-start mb-10">
                    <!--begin::Wrapper-->
                    <div class="d-flex flex-column align-items-start">
                        <!--begin::User-->
                        <div class="d-flex align-items-center mb-2">
                            <!--begin::Details-->
                            <div class="ms-3">
                                <a href="#" class="fs-5 fw-bold text-gray-900 text-hover-primary me-1">اسم المستخدم</a>
                                <span style="margin-right: 5px" class="text-muted fs-7 mb-1">3 دقائق</span>
                            </div>
                            <!--end::Details-->
                        </div>
                        <!--end::User-->
                        <!--begin::Text-->
                        <div class="p-5 rounded bg-light-info text-dark fw-semibold mw-lg-400px"
                             data-kt-element="message-text">اشعار جديد اشعار جديد اشعار جديد اشعار جديد اشعار جديد اشعار
                            جديد اشعار جديد اشعار جديد !!
                        </div>
                        <!--end::Text-->
                        <div style="display: inline-block; margin-top: 10px">
                            <a href="" class="btn btn-primary"><i style="margin: 0 -10px 0 5px"
                                                                  class="fonticon-pin fs-4"></i>
                                اعلام كمقروء</a>
                            <a href="" class="btn btn-primary">
                                <i style="margin: 0 -10px 0 5px" class="fonticon-stats fs-4"></i>
                                التفاصيل</a>
                        </div>
                    </div>
                    <!--end::Wrapper-->
                </div>
                <!--end::Message(in)-->
                <!--begin::Message(in)-->
                <div class="d-flex justify-content-start mb-10">
                    <!--begin::Wrapper-->
                    <div class="d-flex flex-column align-items-start">
                        <!--begin::User-->
                        <div class="d-flex align-items-center mb-2">
                            <!--begin::Details-->
                            <div class="ms-3">
                                <a href="#" class="fs-5 fw-bold text-gray-900 text-hover-primary me-1">اسم المستخدم</a>
                                <span style="margin-right: 5px" class="text-muted fs-7 mb-1">3 دقائق</span>
                            </div>
                            <!--end::Details-->
                        </div>
                        <!--end::User-->
                        <!--begin::Text-->
                        <div class="p-5 rounded bg-light-info text-dark fw-semibold mw-lg-400px"
                             data-kt-element="message-text">اشعار جديد اشعار جديد اشعار جديد اشعار جديد اشعار جديد اشعار
                            جديد اشعار جديد اشعار جديد !!
                        </div>
                        <!--end::Text-->
                        <div style="display: inline-block; margin-top: 10px">
                            <a href="" class="btn btn-primary"><i style="margin: 0 -10px 0 5px"
                                                                  class="fonticon-pin fs-4"></i>
                                اعلام كمقروء</a>
                            <a href="" class="btn btn-primary">
                                <i style="margin: 0 -10px 0 5px" class="fonticon-stats fs-4"></i>
                                التفاصيل</a>
                        </div>
                    </div>
                    <!--end::Wrapper-->
                </div>
                <!--end::Message(in)-->
                <!--begin::Message(in)-->
                <div class="d-flex justify-content-start mb-10">
                    <!--begin::Wrapper-->
                    <div class="d-flex flex-column align-items-start">
                        <!--begin::User-->
                        <div class="d-flex align-items-center mb-2">
                            <!--begin::Details-->
                            <div class="ms-3">
                                <a href="#" class="fs-5 fw-bold text-gray-900 text-hover-primary me-1">اسم المستخدم</a>
                                <span style="margin-right: 5px" class="text-muted fs-7 mb-1">3 دقائق</span>
                            </div>
                            <!--end::Details-->
                        </div>
                        <!--end::User-->
                        <!--begin::Text-->
                        <div class="p-5 rounded bg-light-info text-dark fw-semibold mw-lg-400px"
                             data-kt-element="message-text">اشعار جديد اشعار جديد اشعار جديد اشعار جديد اشعار جديد اشعار
                            جديد اشعار جديد اشعار جديد !!
                        </div>
                        <!--end::Text-->
                        <div style="display: inline-block; margin-top: 10px">
                            <a href="" class="btn btn-primary"><i style="margin: 0 -10px 0 5px"
                                                                  class="fonticon-pin fs-4"></i>
                                اعلام كمقروء</a>
                            <a href="" class="btn btn-primary">
                                <i style="margin: 0 -10px 0 5px" class="fonticon-stats fs-4"></i>
                                التفاصيل</a>
                        </div>
                    </div>
                    <!--end::Wrapper-->
                </div>
                <!--end::Message(in)-->
                <!--begin::Message(in)-->
                <div class="d-flex justify-content-start mb-10">
                    <!--begin::Wrapper-->
                    <div class="d-flex flex-column align-items-start">
                        <!--begin::User-->
                        <div class="d-flex align-items-center mb-2">
                            <!--begin::Details-->
                            <div class="ms-3">
                                <a href="#" class="fs-5 fw-bold text-gray-900 text-hover-primary me-1">اسم المستخدم</a>
                                <span style="margin-right: 5px" class="text-muted fs-7 mb-1">3 دقائق</span>
                            </div>
                            <!--end::Details-->
                        </div>
                        <!--end::User-->
                        <!--begin::Text-->
                        <div class="p-5 rounded bg-light-info text-dark fw-semibold mw-lg-400px"
                             data-kt-element="message-text">اشعار جديد اشعار جديد اشعار جديد اشعار جديد اشعار جديد اشعار
                            جديد اشعار جديد اشعار جديد !!
                        </div>
                        <!--end::Text-->
                        <div style="display: inline-block; margin-top: 10px">
                            <a href="" class="btn btn-primary"><i style="margin: 0 -10px 0 5px"
                                                                  class="fonticon-pin fs-4"></i>
                                اعلام كمقروء</a>
                            <a href="" class="btn btn-primary">
                                <i style="margin: 0 -10px 0 5px" class="fonticon-stats fs-4"></i>
                                التفاصيل</a>
                        </div>
                    </div>
                    <!--end::Wrapper-->
                </div>
                <!--end::Message(in)-->
                <!--begin::Message(in)-->
                <div class="d-flex justify-content-start mb-10">
                    <!--begin::Wrapper-->
                    <div class="d-flex flex-column align-items-start">
                        <!--begin::User-->
                        <div class="d-flex align-items-center mb-2">
                            <!--begin::Details-->
                            <div class="ms-3">
                                <a href="#" class="fs-5 fw-bold text-gray-900 text-hover-primary me-1">اسم المستخدم</a>
                                <span style="margin-right: 5px" class="text-muted fs-7 mb-1">3 دقائق</span>
                            </div>
                            <!--end::Details-->
                        </div>
                        <!--end::User-->
                        <!--begin::Text-->
                        <div class="p-5 rounded bg-light-info text-dark fw-semibold mw-lg-400px"
                             data-kt-element="message-text">اشعار جديد اشعار جديد اشعار جديد اشعار جديد اشعار جديد اشعار
                            جديد اشعار جديد اشعار جديد !!
                        </div>
                        <!--end::Text-->
                        <div style="display: inline-block; margin-top: 10px">
                            <a href="" class="btn btn-primary"><i style="margin: 0 -10px 0 5px"
                                                                  class="fonticon-pin fs-4"></i>
                                اعلام كمقروء</a>
                            <a href="" class="btn btn-primary">
                                <i style="margin: 0 -10px 0 5px" class="fonticon-stats fs-4"></i>
                                التفاصيل</a>
                        </div>
                    </div>
                    <!--end::Wrapper-->
                </div>
                <!--end::Message(in)-->
            </div>
            <!--end::Messages-->
        </div>
        <!--end::Card body-->
    </div>
    <!--end::Messenger-->
</div>
<!--end::Chat drawer-->
