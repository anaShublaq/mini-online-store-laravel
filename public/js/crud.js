function showMessage(data) {
    Swal.fire({
        position: 'center', title: data.title, icon: data.icon, showConfirmButton: false, timer: 1000
    })
}

function resetProductForm() {
    document.getElementById('form_create').reset();
    document.getElementById('basePriceSlider').noUiSlider.reset();
    $(".discountPriceSlider").hide();
}

function store(url, data, formName, submitId) {
    $("#" + submitId).addClass("disabled");
    axios.post(url, data).then(function (response) {
        // handle success 2xx
        console.log(response);
        toastr.success(response.data.message);
        if (formName === 'product') {
            resetProductForm();
        } else {
            document.getElementById('form_create').reset();
        }
        $("#" + submitId).removeClass("disabled");
    }).catch(function (error) {
        // handle error 4xx-5xx
        console.log(error);
        toastr.error(error.response.data.message);
        $("#" + submitId).removeClass("disabled");
    });
}

function redirect(route) {
    if (route != undefined) {
        window.location.href = route;
    }
}

function update(url, data, redirectRoute, submitId) {
    axios.post(url, data).then(function (response) {
        console.log(response);
        redirect(redirectRoute);
        toastr.success(response.data.message);
    }).catch(function (error) {
        // handle error 4xx-5xx
        console.log(error);
        toastr.error(error.response.data.message);
        $('#sendRequest').removeClass("disabled");
    });
}


function confirmDestroy(url, id, reference) {
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert !!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#0d76e3',
        cancelButtonColor: '#da0000',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.isConfirmed) {
            deleteItem(url, id, reference);
            Swal.fire('Deleted!', 'Your data has been deleted.', 'success')
        }
    })
}

function deleteItem(url, id, reference) {
    axios.delete(url + '/' + id)
        .then(function (response) {
            // handle success 2xx
            console.log(response);
            reference.closest('tr').remove();
        })
        .catch(function (error) {
            // handle error 4xx-5xx
            console.log(error);
            showMessage(error.response.data);
        });
}

function confirmPurchase(url, data) {
    Swal.fire({
        title: 'Are you sure?',
        text: "You will purchase product !!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#0d76e3',
        cancelButtonColor: '#da0000',
        confirmButtonText: 'Yes, purchase it!'
    }).then((result) => {
        if (result.isConfirmed) {
            purchaseProduct(url, data);
            Swal.fire('Done !', 'Product has been successfully purchased', 'success')
        }
    })
}

function purchaseProduct(url, data) {
    axios.post(url, data).then(function (response) {
        // handle success 2xx
        console.log(response);
        toastr.success(response.data.message);
    }).catch(function (error) {
        // handle error 4xx-5xx
        console.log(error);
        toastr.error(error.response.data.message);
        showMessage(error.response.data);
    });
}


function slider(sliderId, labelId, min, max) {
    var slider = document.querySelector(sliderId);
    var value = document.querySelector(labelId);

    noUiSlider.create(slider, {
        start: [0], connect: true, range: {
            "min": min, "max": max
        },
    });
    slider.noUiSlider.on("update", function (values, handle) {
        value.innerHTML = Math.round(values[handle]);
        if (handle) {
            value.innerHTML = Math.round(values[handle]);
        }
    });
}

function setSliders() {
    slider("#basePriceSlider", "#basePriceLabel", 1, 1000);
    slider("#discountPriceSlider", "#discountPriceLabel", 0, 0);


    jQuery('#basePriceLabel').bind('DOMSubtreeModified', function (event) {
        let maxDiscountPrice = parseInt(document.getElementById("basePriceLabel").innerHTML);
        document.getElementById('discountPriceSlider').noUiSlider.updateOptions({
            range: {
                min: 0, max: maxDiscountPrice - 1
            }
        });
    });
}
