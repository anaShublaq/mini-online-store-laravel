<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class StoreFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'address' => $this->faker->address(),
            'logo' => $this->faker->imageUrl,
            'created_at' => now(),
            'updated_at' => now(),
            'deleted_at' => NULL
        ];
    }
}
