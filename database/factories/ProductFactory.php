<?php

namespace Database\Factories;

use App\Models\Store;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $stores = Store::pluck('id')->toArray();
        return [
            'name' => $this->faker->name(),
            'description' => $this->faker->sentence(),
            'base_price' => $this->faker->numberBetween(10, 500),
            'discount_price' => $this->faker->numberBetween(10, 500),
            'flag' => $this->faker->randomElement(['0', '1']),
            'store_id' => $this->faker->randomElement($stores),
            'created_at' => now(),
            'updated_at' => now(),
            'deleted_at' => NULL
        ];
    }
}
