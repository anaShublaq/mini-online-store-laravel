<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class PurchaseTransactionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'product_name' => $this->faker->name(),
            'purchase_price' => $this->faker->numberBetween(1, 500),
            'created_at' => now(),
            'updated_at' => now(),
            'deleted_at' => NULL
        ];
    }
}
