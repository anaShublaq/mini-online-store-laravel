<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::with('store')->latest()->paginate(5);
        return response()->view('cms/products/index', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->view('cms/products/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator($request->all(), [
            'name' => 'required|String|min:3|max:30',
            'description' => 'nullable|String|min:3|max:200',
            'storeId' => 'exists:stores,id',
            'basePriceLabel' => 'nullable|min:1|gt:discountPriceLabel',
            'discountPriceLabel' => 'nullable|min:0',
            'logo' => 'nullable|image|mimes:jpg,jpeg,png|max:2048'
        ], [
            'storeId.exists' => 'You must choose store !!',
            'basePriceLabel.gt' => 'Discount price must be smaller than base price !!'
        ]);

        if (!$validator->fails()) {
            $product = new Product();
            $product->name = $request->get('name');
            $product->description = $request->get('description');
            $product->base_price = $request->get('basePriceLabel');
            $product->discount_price = $request->get('discountPriceLabel');

            if ($request->get('hasDiscountPrice')) {
                $product->flag = '1';
            } else {
                $product->flag = '0';
            }

            if ($request->hasFile('logo')) {
                $logo = $request->file('logo');
                $path = 'products_logo/';
                $logoName = time() . random_int(1, 10000000000) . '_' . $product->name . '.' . $logo->getClientOriginalExtension();
                $logo->storePubliclyAs($path, $logoName, ['disk' => 'public']);
                $product->logo = $path . $logoName;
            }

            $product->store_id = $request->get('storeId');
            $isSaved = $product->save();
            return response()->json([
                'message' => $isSaved ? 'Saved Successfully' : 'Failed to add new product'
            ], $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        } else {
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findorfail($id);
        return response()->view('cms/products/edit', ['product' => $product]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Product $product)
    {
        $validator = Validator($request->all(), [
            'name' => 'required|String|min:3|max:30',
            'description' => 'nullable|String|min:3|max:200',
            'storeId' => 'exists:stores,id',
            'basePriceLabel' => 'nullable|min:1|gt:discountPriceLabel',
            'discountPriceLabel' => 'nullable|min:0',
            'logo' => 'nullable|image|mimes:jpg,jpeg,png|max:2048'
        ], [
            'storeId.exists' => 'You must choose store !!',
            'basePriceLabel.gt' => 'Discount price must be smaller than base price !!'
        ]);

        if (!$validator->fails()) {
            $product->name = $request->get('name');
            $product->description = $request->get('description');
            $product->base_price = $request->get('basePriceLabel');
            $product->discount_price = $request->get('discountPriceLabel');

            if ($request->get('hasDiscountPrice')) {
                $product->flag = '1';
            } else {
                $product->flag = '0';
            }

            if ($request->hasFile('logo')) {
                $path = 'products_logo/';
                Storage::disk('public')->delete($product->logo);
                $logo = $request->file('logo');
                $logoName = time() . random_int(1, 10000000000) . '_' . $product->name . '.' . $logo->getClientOriginalExtension();
                $logo->storePubliclyAs($path, $logoName, ['disk' => 'public']);
                $product->logo = $path . $logoName;
            }

            $product->store_id = $request->get('storeId');
            $isSaved = $product->save();
            return response()->json([
                'message' => $isSaved ? 'Saved Successfully' : 'Failed to add new product'
            ], $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        } else {
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $isDeleted = Product::findorfail($id)->delete();
        return response()->json([
            'icon' => 'success',
            'title' => $isDeleted ? 'Deleted Successfully' : 'Failed to delete product !!'
        ], $isDeleted ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST);
    }
}
