<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stores = Store::latest()->paginate(5);
        return response()->view('cms.stores.index', ['stores' => $stores]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->view('cms.stores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator($request->all(), [
            'name' => 'required|string|min:3|max:50',
            'address' => 'nullable|string|min:3|max:200',
            'logo' => 'nullable|image|mimes:jpg,jpeg,png|max:2048'
        ]);

        if (!$validator->fails()) {
            $store = new Store();
            $store->name = $request->get('name');
            $store->address = $request->get('address');

            if ($request->hasFile('logo')) {
                $logo = $request->file('logo');
                $path = 'stores_logo/';
                $logoName = time() . random_int(1, 10000000000) . '_' . $store->name . '.' . $logo->getClientOriginalExtension();
                $logo->storePubliclyAs($path, $logoName, ['disk' => 'public']);
                $store->logo = $path . $logoName;
            }
            $isSaved = $store->save();
            return response()->json([
                'message' => $isSaved ? 'Saved Successfully' : 'Faild to add new store'
            ], $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        } else {
            return \response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Store $store
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Store $store
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $store = Store::findorfail($id);
        return response()->view('cms/stores/edit', ['store' => $store]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Store $store
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Store $store)
    {

        $validator = Validator($request->all(), [
            'name' => 'required|string|min:3|max:50',
            'address' => 'nullable|string|min:3|max:200',
            'logo' => 'nullable|image|mimes:jpg,jpeg,png|max:2048'
        ]);

        if (!$validator->fails()) {
            $store->name = $request->get('name');
            $store->address = $request->get('address');

            if ($request->hasFile('logo')) {
                $path = 'stores_logo/';
                Storage::disk('public')->delete($store->logo);
                $logo = $request->file('logo');
                $logoName = time() . random_int(1, 10000000000) . '_' . $store->name . '.' . $logo->getClientOriginalExtension();
                $logo->storePubliclyAs($path, $logoName, ['disk' => 'public']);
                $store->logo = $path . $logoName;
            }
            $isSaved = $store->save();
            return response()->json([
                'message' => $isSaved ? 'Edit Successfully' : 'Faild to edit store'
            ], $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        } else {
            return \response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Store $store
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $isDeleted = Store::findorfail($id)->delete();
        return response()->json([
            'icon' => 'success',
            'title' => $isDeleted ? 'Deleted Successfully' : 'Failed to delete store !!'
        ], $isDeleted ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST);
    }
}
