<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\Models\PurchaseTransaction;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class PurchaseTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $purchase_transactions = PurchaseTransaction::latest()->paginate(5);
        return response()->view('cms/purchase_transactions/index', ['purchase_transactions' => $purchase_transactions]);
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function ViewReport()
    {
        $report_info = PurchaseTransaction::query()->select(DB::raw(
            'DISTINCT(count(product_name)) AS product_count ,
                 SUM(purchase_price) AS total_price,
                 product_name'))
            ->groupBy('product_name')
            ->paginate(5);
        return response()->view('cms/purchase_transactions/report', ['report_info' => $report_info]);
    }
}
