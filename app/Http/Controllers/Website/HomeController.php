<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\PurchaseTransaction;
use App\Models\Store;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function show()
    {
        $storesCount = Store::count();
        $productsCount = Product::count();
        $purchaseTransactions = PurchaseTransaction::count();
        return response()->view('website/home', [
            'storesCount' => $storesCount,
            'productsCount' => $productsCount,
            'purchaseTransactions' => $purchaseTransactions
        ]);
    }
}
