<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use PHPUnit\Exception;

class ProductsController extends Controller
{
    public function show($id, Request $request)
    {
        $store = Store::with(['products' => function ($q) use ($request) {
            return $q->when($request->keyword, function ($q) use ($request) {
                $q->where('name', 'LIKE', '%' . $request->keyword . '%');
            });
        }])->findorfail($id);

        return response()->view('website/products', ['store' => $store]);
    }
}
