<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\PurchaseTransaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PurchaseTransactionController extends Controller
{
    public function purchase(Request $request)
    {
        $newPurchaseTransaction = new PurchaseTransaction();
        $newPurchaseTransaction->store_name = $request->get('storeName');
        $newPurchaseTransaction->product_name = $request->get('productName');
        $newPurchaseTransaction->purchase_price = $request->get('purchasePrice');
        $newPurchaseTransaction->transaction_time = Carbon::now();
        $isSaved = $newPurchaseTransaction->save();

        return response()->json([
            'message' => $isSaved ? 'Product successfully purchased' : 'Failed to purchased product !!'
        ], $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
    }
}
