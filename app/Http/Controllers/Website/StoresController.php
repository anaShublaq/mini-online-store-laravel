<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\Store;
use Illuminate\Http\Request;

class StoresController extends Controller
{
    public function show()
    {
        $stores = Store::latest()->with('products')->paginate(6);
        return response()->view('website/stores', ['stores' => $stores]);
    }
}
