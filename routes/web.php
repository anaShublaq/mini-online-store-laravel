<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/pages/home', function () {
    return view('pages/home');
})->middleware('auth')->name('page.home');

Route::prefix('cms/')->middleware('auth')->group(function () {
    Route::resource('store', \App\Http\Controllers\Cms\StoreController::class);
    Route::resource('product', \App\Http\Controllers\Cms\ProductController::class);
    Route::resource('purchaseTransaction', \App\Http\Controllers\Cms\PurchaseTransactionController::class);
    Route::get('transactions_report', [\App\Http\Controllers\Cms\PurchaseTransactionController::class, 'ViewReport'])->name('report');
});

Route::prefix('website/')->group(function () {
    Route::get('home', [App\Http\Controllers\Website\HomeController::class, 'show'])->name('website.home');
    Route::get('stores', [App\Http\Controllers\Website\StoresController::class, 'show'])->name('website.stores');
    Route::get('products_store/{id}', [App\Http\Controllers\Website\ProductsController::class, 'show'])->name('website.products');
    Route::post('purchase_transaction', [App\Http\Controllers\Website\PurchaseTransactionController::class, 'purchase'])->name('website.product.purchase');
});

Route::prefix('website/')->group(function () {
    Route::post('/stores/search', [\App\Http\Controllers\Website\StoresController::class, 'show'])->name('store.search');
    Route::post('/products/search/{id}', [\App\Http\Controllers\Website\ProductsController::class, 'show'])->name('product.search');
});
